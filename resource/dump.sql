-- Adminer 4.1.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `cart`;
CREATE TABLE `cart` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `quantity` smallint(5) NOT NULL,
  `product_id` int(10) NOT NULL,
  `cart_order_id` int(10) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cart_order_id` (`cart_order_id`),
  CONSTRAINT `cart_ibfk_1` FOREIGN KEY (`cart_order_id`) REFERENCES `cart_order` (`id`),
  CONSTRAINT `cart_ibfk_2` FOREIGN KEY (`cart_order_id`) REFERENCES `cart_order` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `cart` (`id`, `user_id`, `quantity`, `product_id`, `cart_order_id`, `date_created`, `last_updated`) VALUES
  (33,	2,	2,	2,	26,	'2015-07-19 12:17:37',	'2015-07-19 12:17:37'),
  (34,	2,	2,	1,	26,	'2015-07-19 12:17:37',	'2015-07-19 12:17:37');

DROP TABLE IF EXISTS `cart_order`;
CREATE TABLE `cart_order` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `delivery_country` varchar(25) COLLATE utf8_czech_ci DEFAULT NULL,
  `delivery_city` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `delivery_address` varchar(75) COLLATE utf8_czech_ci DEFAULT NULL,
  `delivery_zip` varchar(25) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_country` varchar(25) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_city` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_address` varchar(75) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_zip` varchar(25) COLLATE utf8_czech_ci DEFAULT NULL,
  `shipping` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `payment` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `state` enum('ADDRESS','SHIPPING','PAYMENT') COLLATE utf8_czech_ci DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `cart_order` (`id`, `delivery_country`, `delivery_city`, `delivery_address`, `delivery_zip`, `billing_country`, `billing_city`, `billing_address`, `billing_zip`, `shipping`, `payment`, `state`, `date_created`, `last_updated`) VALUES
  (26,	'CR',	'BB',	'ASAD',	'asa',	'CR',	'BB',	'ASAD',	'asa',	NULL,	NULL,	'SHIPPING',	'2015-07-19 12:17:37',	'2015-07-19 13:18:48');

DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `value` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `date_created` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `config` (`id`, `name`, `value`, `date_created`, `last_updated`) VALUES
  (1,	'web-name',	'RPI shop',	'2015-07-15 22:57:18',	'2015-07-15 22:57:18'),
  (2,	'favicon',	'1436993843.png',	'2015-07-15 22:57:23',	'2015-07-15 22:57:23'),
  (3,	'logo',	'1436993847.jpg',	'2015-07-15 22:57:27',	'2015-07-15 22:57:27'),
  (4,	'slider',	'1436997114.png',	'2015-07-15 22:57:40',	'2015-07-15 23:51:54');

DROP TABLE IF EXISTS `image`;
CREATE TABLE `image` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `type` enum('SLIDER','FILE') COLLATE utf8_czech_ci NOT NULL,
  `sort` smallint(5) NOT NULL,
  `date_created` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `image` (`id`, `name`, `slug`, `type`, `sort`, `date_created`, `last_updated`) VALUES
  (2,	'1436997572.png',	'1436997572.png',	'SLIDER',	0,	'2015-07-15 23:59:32',	'2015-07-19 14:46:52'),
  (4,	'1437043611.png',	'1437043611.png',	'SLIDER',	1,	'2015-07-16 12:46:51',	'2015-07-19 14:47:03'),
  (5,	'1437309852.png',	'1437309852.png',	'SLIDER',	2,	'2015-07-19 14:44:12',	'2015-07-19 14:47:03'),
  (6,	'1437309856.png',	'1437309856.png',	'SLIDER',	0,	'2015-07-19 14:44:16',	'2015-07-19 14:47:03'),
  (7,	'14373130291435939566-5f5cecaf092f23267371b22a23544aa1.png',	'14373130291435939566-5f5cecaf092f23267371b22a23544aa1.png',	'',	0,	'2015-07-19 15:37:09',	'2015-07-19 15:37:09'),
  (8,	'14373130781435939509-c966fadd10ec100510a71c0fda9a4d07.png',	'14373130781435939509-c966fadd10ec100510a71c0fda9a4d07.png',	'FILE',	0,	'2015-07-19 15:37:58',	'2015-07-19 15:37:58');

DROP TABLE IF EXISTS `navigation`;
CREATE TABLE `navigation` (
  `id` smallint(5) NOT NULL AUTO_INCREMENT,
  `parent_id` smallint(5) DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `text` text COLLATE utf8_czech_ci NOT NULL,
  `type` enum('home','eshop','classic') COLLATE utf8_czech_ci NOT NULL,
  `margin_left` smallint(5) NOT NULL,
  `size` smallint(5) NOT NULL,
  `sort` smallint(5) NOT NULL,
  `keywords` varchar(150) COLLATE utf8_czech_ci NOT NULL,
  `description` text COLLATE utf8_czech_ci NOT NULL,
  `date_created` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `navigation` (`id`, `parent_id`, `name`, `slug`, `text`, `type`, `margin_left`, `size`, `sort`, `keywords`, `description`, `date_created`, `last_updated`) VALUES
  (1,	NULL,	'HLAVNÍ STRANA',	'hlavni-strana',	'',	'home',	0,	100,	1,	'',	'',	'2015-07-15 22:58:38',	'2015-07-15 23:28:01'),
  (2,	NULL,	'DOPRAVA A PLATBA ZBOŽÍ',	'doprava-a-platba-zbozi',	'<p><strong>Platba:</strong><br /><br />Objedn&aacute;vky lze platit n&aacute;sleduj&iacute;c&iacute;mi způsoby:</p>\n<p><strong>Bankovn&iacute;m převodem</strong> - Platbu je potřeba zaslat na z&aacute;kladě vystaven&eacute; faktury na n&aacute;&scaron; &uacute;čet u AirBank. Z&aacute;silka je odesl&aacute;na v den přijet&iacute; platby (Pokud nen&iacute; domluveno jinak.) Bankovn&iacute; převod trv&aacute; zpravidla 1-2 pracovn&iacute; dny.<br /><br /><strong>Dob&iacute;rkou</strong> -&nbsp; V př&iacute;padě dob&iacute;rky, je č&aacute;stka za zbož&iacute; hrazena hotově při převzen&iacute; z&aacute;silky. K objedn&aacute;vk&aacute;m je přičteno dob&iacute;rečn&eacute; 30 Kč (Vyjma osobn&iacute;mu odběru v Česk&yacute;ch Budějovic&iacute;ch.)</p>\n<p><strong>Rychl&aacute; on-line platba pomoc&iacute; Gopay</strong> - Platebn&iacute; br&aacute;na umožnuje platbu platebn&iacute;mi kartami Visa/MasterCard a platebn&iacute;mi br&aacute;nami Raiffeisen Bank, KB, mBank, Fio a Česk&eacute; Spořitelny. <strong>Registrace u GoPay nen&iacute; nutn&aacute;.</strong> Platit lze v Česk&yacute;ch korun&aacute;ch, nebo Eurem. Platba přes platebn&iacute; br&aacute;nu je evidov&aacute;na obratem. Platbu lze opakovat přes administraci Va&scaron;eho &uacute;čtu a z odkazů, kter&eacute; najdete v emailu oznamuj&iacute;c&iacute;m ne&uacute;spě&scaron;nou platbu.&nbsp; V př&iacute;padě probl&eacute;mů s platebn&iacute; br&aacute;nou se na n&aacute;s nev&aacute;hejte obr&aacute;tit, r&aacute;di V&aacute;m s dokončen&iacute;m objedn&aacute;vky pomůžeme.</p>\n<p><strong>Doprava:</strong></p>\n<p>Před&aacute;n&iacute; zbož&iacute; je možno realizovat n&iacute;že uveden&yacute;mi způsoby. Pokud by V&aacute;m ani jeden&nbsp;z uveden&yacute;ch způsobů nevyhovoval, je možn&eacute; se přes na&scaron;i z&aacute;kaznickou podporu domluvit na individu&aacute;ln&iacute;m ře&scaron;en&iacute;.&nbsp;</p>\n<p><strong>Z&aacute;silky jsou odes&iacute;l&aacute;ny standardně v den n&aacute;kupu.<br /><br />N&aacute;kupy po 16 hodině nejpozději den n&aacute;sleduj&iacute;c&iacute;.</strong>&nbsp;</p>\n<p>Objedn&aacute;vky realizovan&aacute; v p&aacute;tek po 16 hodině s doručen&iacute;m Českou Po&scaron;tou jsou odes&iacute;l&aacute;ny o v&iacute;kendu.</p>\n<p><strong>Od 2 000 Kč je po&scaron;tovn&eacute; zdarma. Detaily naleznete n&iacute;že u jednotliv&yacute;ch dopravců.<br /><br /></strong><strong>Vět&scaron;ina objedn&aacute;vek doraz&iacute; v př&iacute;padě n&aacute;kupu do 16:00 do 24 hodin od objedn&aacute;n&iacute;/zaplacen&iacute;.&nbsp;</strong></p>\n<p><strong>DPD - Česk&aacute; Republika</strong></p>\n<p>DPD Private - ČR (platba předem)&nbsp;- 75 Kč -&nbsp;<strong>Při n&aacute;kupu nad 5 000 Kč ZDARMA</strong><br />DPD Private - ČR&nbsp;(dob&iacute;rka) - 105 Kč&nbsp;-&nbsp;<strong>Při n&aacute;kupu nad 5 000 Kč pouze 30 Kč</strong>&nbsp;</p>\n<p><strong>Česk&aacute; po&scaron;ta</strong></p>\n<p>Doporučen&aacute; z&aacute;silka (platba předem)&nbsp;- 66 Kč -&nbsp;<strong>Při n&aacute;kupu nad 2 000 Kč ZDARMA</strong><br />Doporučen&aacute; z&aacute;silka (dob&iacute;rka) - 96 Kč&nbsp;-&nbsp;<strong>Při n&aacute;kupu nad 2 000 Kč pouze 30&nbsp;Kč</strong><br />Bal&iacute;k Do ruky (platba předem)&nbsp;- 95 Kč&nbsp;<br />Bal&iacute;k Do ruky&nbsp;(dob&iacute;rka) - 125 Kč&nbsp;<br />*Doporučen&aacute; z&aacute;silka (Nově - Doporučen&eacute; psan&iacute;) je dostupn&aacute; pouze u objedn&aacute;vek do 5 000 Kč a je limitov&aacute;na pouze na men&scaron;&iacute; bal&iacute;ky do 5cm tlou&scaron;ťky, 50cm &scaron;&iacute;řky, 35cm v&yacute;&scaron;ky a 2kg v&aacute;hy. V př&iacute;padě zakoupen&iacute; nevhodn&eacute;ho zbož&iacute; pro tuto dopravu, nen&iacute; tato varianta automaticky v ko&scaron;&iacute;ku nab&iacute;zena.&nbsp;</p>\n<p><strong>Osobn&iacute; odběr v Česk&yacute;ch Budějovic&iacute;ch</strong></p>\n<p>Osobn&iacute; odběr&nbsp;(platba předem)&nbsp;- ZDARMA!<br />Osobn&iacute; odběr (dob&iacute;rka) -&nbsp;ZDARMA!&nbsp;</p>\n<p><strong>Ulozenka.cz, Heureka.point&nbsp;</strong></p>\n<p>Osobn&iacute; odběr, HeurekaPoint/Ulozenka.cz (platba předem) - 40 Kč<br />Osobn&iacute; odběr, HeurekaPoint/Ulozenka.cz (dob&iacute;rka) - 70 Kč&nbsp;</p>\n<p>Uloženka je s&iacute;ť v&yacute;dejn&iacute;ch m&iacute;st. Z&aacute;silky se nedoručuj&iacute; na dodac&iacute; adresu, ale zbož&iacute; V&aacute;m bude zasl&aacute;no na V&aacute;mi zvolenou pobočku.</p>\n<p>&nbsp;<strong>Slovensk&aacute; po&scaron;ta</strong></p>\n<p>Slovensk&aacute; po&scaron;ta ,&nbsp;smluvn&iacute; bal&iacute;k&nbsp;(platba předem)&nbsp;- 113 Kč&nbsp;<br />Slovensk&aacute; po&scaron;ta , smluvn&iacute; bal&iacute;k (dob&iacute;rka) - 143 Kč&nbsp;</p>',	'classic',	0,	100,	4,	'',	'',	'2015-07-15 23:00:26',	'2015-07-15 23:43:04'),
  (3,	NULL,	'RASPBERRY PI - PŘÍSLUŠENSTVÍ',	'raspberry-pi-prislusenstvi',	'',	'eshop',	18,	75,	2,	'',	'',	'2015-07-15 23:00:42',	'2015-07-15 23:29:30'),
  (5,	3,	'Case, krabičky, obaly',	'case-krabicky-obaly',	'',	'eshop',	0,	100,	1,	'',	'',	'2015-07-15 23:15:50',	'2015-07-15 23:15:50'),
  (6,	3,	'Zdroje, napájení, baterie',	'zdroje-napajeni-baterie',	'',	'eshop',	0,	100,	2,	'',	'',	'2015-07-15 23:16:21',	'2015-07-15 23:16:21'),
  (7,	3,	'Paměťové karty',	'pametove-karty',	'',	'eshop',	0,	100,	3,	'',	'',	'2015-07-15 23:16:38',	'2015-07-15 23:16:38'),
  (8,	3,	'A/V kabely, redukce, LAN, Wifi',	'a-v-kabely-redukce-lan-wifi',	'',	'eshop',	0,	100,	5,	'',	'',	'2015-07-15 23:16:52',	'2015-07-15 23:21:54'),
  (9,	3,	'Rozšiřující moduly',	'rozsirujici-moduly',	'',	'eshop',	0,	100,	4,	'',	'',	'2015-07-15 23:17:07',	'2015-07-15 23:21:54'),
  (10,	3,	'Chlazení',	'chlazeni',	'',	'eshop',	0,	100,	8,	'',	'',	'2015-07-15 23:17:28',	'2015-07-15 23:24:41'),
  (11,	3,	'USB příslušenství',	'usb-prislusenstvi',	'',	'eshop',	0,	100,	7,	'',	'',	'2015-07-15 23:18:00',	'2015-07-15 23:18:00'),
  (14,	NULL,	'RPI SADY',	'rpi-sady',	'',	'eshop',	50,	25,	3,	'',	'',	'2015-07-15 23:30:10',	'2015-07-15 23:32:23'),
  (15,	14,	'Sada1',	'sada1',	'',	'eshop',	0,	100,	1,	'',	'',	'2015-07-15 23:30:29',	'2015-07-15 23:30:29'),
  (16,	14,	'Sada 2',	'sada-2',	'',	'eshop',	0,	100,	2,	'',	'',	'2015-07-15 23:30:56',	'2015-07-15 23:30:56');

DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `category_id` smallint(5) NOT NULL,
  `price` int(10) NOT NULL,
  `status` enum('selling','notselling') COLLATE utf8_czech_ci NOT NULL,
  `stock` smallint(5) NOT NULL,
  `description` text COLLATE utf8_czech_ci NOT NULL,
  `keywords` varchar(150) COLLATE utf8_czech_ci NOT NULL,
  `date_created` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `product` (`id`, `name`, `slug`, `category_id`, `price`, `status`, `stock`, `description`, `keywords`, `date_created`, `last_updated`) VALUES
  (1,	'Raspberry PI 2',	'raspberry-pi-2',	5,	1002,	'selling',	24,	'<p>6x v&yacute;konněj&scaron;&iacute;,nejmen&scaron;&iacute; poč&iacute;tač na světě ve variantě se 4USB porty, 40pin GPIO, microSD kartami!</p>',	'',	'2015-07-16 00:05:21',	'2015-07-18 22:08:16'),
  (2,	'test product',	'test-product',	6,	1234,	'selling',	21,	'<p>s fsafsdfdas fs fsafsdfdas fs fsafsdfdas fs fsafsdfdas fs fsafsdfdas f</p>',	'',	'2015-07-18 10:39:06',	'2015-07-18 22:07:50'),
  (3,	'test product 4',	'test-product-4',	8,	3232,	'notselling',	32,	'',	'',	'2015-07-18 22:07:02',	'2015-07-19 14:38:22');

DROP TABLE IF EXISTS `product_image`;
CREATE TABLE `product_image` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `slug` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `product_id` int(10) NOT NULL,
  `title` tinyint(1) NOT NULL,
  `date_created` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `product_image` (`id`, `slug`, `product_id`, `title`, `date_created`, `last_updated`) VALUES
  (1,	'1436997921_0.jpg',	1,	1,	'2015-07-16 00:05:21',	'2015-07-16 00:05:21'),
  (2,	'1437249956_0.png',	2,	1,	'2015-07-18 22:05:56',	'2015-07-18 22:05:56'),
  (3,	'1437250070_0.jpg',	2,	0,	'2015-07-18 22:07:50',	'2015-07-18 22:07:50'),
  (4,	'1437250070_1.jpg',	2,	0,	'2015-07-18 22:07:50',	'2015-07-18 22:07:50'),
  (5,	'1437250096_0.jpg',	1,	0,	'2015-07-18 22:08:16',	'2015-07-18 22:08:16');

DROP TABLE IF EXISTS `right_box`;
CREATE TABLE `right_box` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `name_allowed` tinyint(1) NOT NULL,
  `text` text COLLATE utf8_czech_ci NOT NULL,
  `type` enum('classic','discount') COLLATE utf8_czech_ci NOT NULL,
  `sort` smallint(5) NOT NULL,
  `date_created` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `right_box` (`id`, `name`, `name_allowed`, `text`, `type`, `sort`, `date_created`, `last_updated`) VALUES
  (1,	'SLEVA',	1,	'',	'discount',	1,	'2015-07-15 23:44:22',	'2015-07-15 23:49:36'),
  (4,	'DOPORUČENÉ ODKAZY',	1,	'<p><strong><span style=\"font-size: 14px;\"><a href=\"https://www.maxiorel.cz/tagy/raspberry-pi\">maxiorel.cz | Čl&aacute;nky o Raspberry PI</a></span></strong></p>\n<p><strong><span style=\"font-size: 14px;\"><a href=\"http://cdr.cz/clanek/raspberry-pi-2-recenze\">cdr.cz | Recenze Raspberry PI 2</a></span></strong></p>\n<p><strong><span style=\"font-size: 14px;\"><a href=\"http://rpiblog.cz/\">RPiBlog.cz | Česk&yacute; blog o Raspoberry PI</a></span></strong></p>',	'classic',	2,	'2015-07-15 23:48:10',	'2015-07-15 23:49:36'),
  (6,	'INFORMACE',	1,	'<p><strong><span style=\"font-size: 14px;\">V&scaron;eobecn&eacute; obchodn&iacute; podm&iacute;nky</span></strong></p>\n<p><strong><span style=\"font-size: 14px;\">Doprava a platba zbož&iacute;</span></strong></p>\n<p><strong><span style=\"font-size: 14px;\">Věrnostn&iacute; program</span></strong></p>\n<p><strong><span style=\"font-size: 14px;\">Na&scaron;e prodejny</span></strong></p>',	'classic',	3,	'2015-07-15 23:51:27',	'2015-07-15 23:51:27');

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(75) COLLATE utf8_czech_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `role` enum('user','admin') COLLATE utf8_czech_ci NOT NULL,
  `country` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `city` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `address` varchar(75) COLLATE utf8_czech_ci DEFAULT NULL,
  `zip` varchar(15) COLLATE utf8_czech_ci DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `user` (`id`, `first_name`, `last_name`, `email`, `password`, `enabled`, `role`, `country`, `city`, `address`, `zip`, `date_created`, `last_updated`) VALUES
  (1,	'admin',	'admin',	'test',	'82b9005ce5e5fa60a9a0359bb9ac5bd611874ebe',	1,	'admin',	'',	'',	'',	'',	'2015-07-15 22:56:08',	'2015-07-16 13:38:22'),
  (2,	'Lukas',	'Fessl',	'lukasfessl@email.cz',	'82b9005ce5e5fa60a9a0359bb9ac5bd611874ebe',	1,	'user',	'CR',	'BB',	'ASAD',	'asa',	'2015-07-15 22:56:57',	'2015-07-15 22:56:57'),
  (3,	'John',	'Doe',	'dd',	'82b9005ce5e5fa60a9a0359bb9ac5bd611874ebe',	1,	'user',	'',	'',	'',	'',	'2015-07-16 22:42:02',	'2015-07-16 22:42:02'),
  (4,	'John',	'Doe',	'ddd',	'82b9005ce5e5fa60a9a0359bb9ac5bd611874ebe',	1,	'user',	'Česká republika',	'České Budějovice',	'Test1',	'37001',	'2015-07-16 22:42:56',	'2015-07-16 22:42:56'),
  (5,	'Lukas2',	'Fessl2',	'lukasfessl@gmail.com',	'82b9005ce5e5fa60a9a0359bb9ac5bd611874ebe',	1,	'user',	'',	'',	'',	'',	'2015-07-18 10:37:49',	'2015-07-18 10:37:49');

-- 2015-07-19 13:41:08