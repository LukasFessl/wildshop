<?php

namespace AdminModule;

use Nette;
use Nette\Application\UI\Form;
use Nette\Application\UI\Control;
use Nette\Utils\Strings;
use Bean\ORM\Create;

class TextForm extends Control
{
    private $type;

    public function __construct($type)
    {
        $this->type = $type;
    }


    public function createComponentForm()
    {
        $eshopName = Create::Config()->findByName($this->type);

        $form = new Form();
        $form->addText('name', 'Název')->setValue($eshopName->value);
        $form->addSubmit('send', 'Uložit');
        $form->onSuccess[] = $this->textProcess;
        return $form;
    }



    public function textProcess($form)
    {
        $val = $form->getValues();

        $config = Create::Config()->findByName($this->type);
        $config->name = $this->type;
        $config->value = $val['name'];
        $config->save();
        $this->presenter->redirect('this');

    }


    public function render()
    {
        $this->template->setFile(__DIR__.'/Form.latte');
        $this->template->render();
    }
}
