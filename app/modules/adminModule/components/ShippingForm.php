<?php

namespace AdminModule;

use Nette;
use Nette\Application\UI\Form;
use Nette\Application\UI\Control;
use Nette\Utils\Strings;
use Bean\ORM\Create;

class ShippingForm extends Control
{
    private $shippingType;

    public function __construct($shippingType)
    {
        $this->shippingType = $shippingType;
    }

    public function createComponentForm()
    {
        $form = new Form();
        $form->addText('name', 'Název')->setValue($this->shippingType->name);
        $form->addText('price', 'Cena')->setValue($this->shippingType->price);
        $form->addSubmit('send', 'Uložit');
        $form->onSuccess[] = $this->textProcess;
        return $form;
    }



    public function textProcess($form)
    {
        $val = $form->getValues();

        $shippingType = $this->shippingType;
        $shippingType->bind($val);
        $shippingType->save();
        $this->presenter->redirect('this');

    }


    public function render()
    {
        $this->template->setFile(__DIR__.'/Form.latte');
        $this->template->render();
    }
}
