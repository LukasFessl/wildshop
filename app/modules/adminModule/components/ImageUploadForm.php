<?php

namespace AdminModule;

use Model\Entity\Image;
use Nette;
use Nette\Application\UI\Form;
use Nette\Application\UI\Control;
use Nette\Utils\Strings;
use Bean\ORM\Create;

class ImageUploadForm extends Control
{
	private $type;
    private $layout;

	public function __construct($type, $layout = 'Form')
	{
		$this->type = $type;
        $this->layout = $layout;
	}


	public function createComponentForm()
	{

		$form = new Form();
        $form->addUpload('file', 'Soubor');
        $form->addSubmit('send', 'Uložit');
        if($this->type == Image::SLIDER)
            $form->onSuccess[] = $this->sliderProcess;
        else
            $form->onSuccess[] = $this->fileProcess;
		return $form;
	}



	public function fileProcess($form)
	{
        $val = $form->getValues();

        $fileEnd = Strings::match($val['file']->name, '~\.jpg$|\.jpeg$|\.png$|\.gif$~i');
        $fileEnd = $fileEnd[0];

        if ($val['file']->isOk() && $fileEnd) {
            $config = Create::Config()->findByName($this->type);

            if ($config->id && file_exists('upload/images/' . $config->name)) {
                unlink('upload/images/' . $config->name);
            }

            $config->name = $this->type;
            $config->value = time().$fileEnd;

            if($config->save()) {
                $val['file']->move('upload/images/'.$config->value);
                $this->presenter->flashMessage("Soubor byl vložen");
                $this->presenter->redirect('this');
            }
        } else {
            if(!$val['file']->isOk())
                $form->addError('Při nahrávání souboru došlo k neznámé chybě');
            if($fileEnd)
                $form->addError('Soubor musí být formátu jpg, png nebo gif');
        }
	}


    public function sliderProcess($form)
    {
        $val = $form->getValues();

        $fileEnd = Strings::match($val['file']->name, '~\.jpg$|\.jpeg$|\.png$|\.gif$~i');
        $fileEnd = $fileEnd[0];

        if ($val['file']->isOk() && $fileEnd) {
            $image = Create::Image();
            $image->name = time().$fileEnd;
            $image->slug = time().$fileEnd;
            $image->type = Image::SLIDER;
            $image->sort = 0;

            if($image->save()) {
                $val['file']->move('upload/images/'.$image->slug);
                $this->presenter->flashMessage("Soubor byl vložen");
                $this->presenter->redirect('this');
            }
        } else {
            if(!$val['file']->isOk())
                $form->addError('Při nahrávání souboru došlo k neznámé chybě');
            if($fileEnd)
                $form->addError('Soubor musí být formátu jpg, png nebo gif');
        }
    }




	public function render()
	{
		$this->template->setFile(__DIR__.'/'.$this->layout.'.latte');
		$this->template->render();
	}
}
