<?php

namespace AdminModule;

use Nette;
use Nette\Application\UI\Form;
use Nette\Application\UI\Control;
use Nette\Utils\Strings;
use Bean\ORM\Create;

class NavigationForm extends Control
{
    private $navigation;

	public function __construct($navigation = null)
	{
        $this->navigation = $navigation;
	}


	public function createComponentForm()
	{

        $navItems = array();
        $setValueForSelect = NULL;
        $nav = create::navigation()->findAll();
        $homeIsSelect = false;
        foreach($nav as $navItem) {
            if($navItem->parentId === NULL)
                $navItems[$navItem->id] = $navItem->name;
            if($this->navigation->parentId == $navItem->id)
                $setValueForSelect = $navItem;
            if($navItem->type == 'home' && $navItem->id != $this->presenter->getParameter('id'))
                $homeIsSelect = true;
        }

        $homeIsSelect == false ? $types['home'] = 'home' : "";
        $types['classic'] = 'classic';
        $types['eshop'] = 'Eshop';


		$form = new Form();
        $form->addText('name', 'Název')->setValue($this->navigation->name);
        if ($this->navigation->id == NULL || $this->navigation->parentId == NULL)
            $form->addSelect('parent_id', 'Rodič', $navItems)->setPrompt("Bez rodiče");
        else
            $form->addSelect('parent_id', 'Rodič', $navItems)->setValue($setValueForSelect->id);
        $form->addSelect('type', 'Typ stránky', $types)->setValue($this->navigation->type);
        $form->addTextArea('text', 'Text')->setAttribute('id', 'textarea_mce_navigation')->setValue($this->navigation->text);
        $form->addText('margin_left', 'Odsazení menu z leva')->setValue($this->navigation->marginLeft);
        $form->addText('size', 'Šířka menu')->setValue($this->navigation->size);
        $form->addText('keywords', 'Klíčová slova')->setValue($this->navigation->keywords);
        $form->addText('description', 'Meta popis')->setValue($this->navigation->description);
        $form->addSubmit('send', 'Uložit');
        $form->onSuccess[] = $this->productProcess;
		return $form;
	}


    // TODO v jedne subkategorii by nemelo jit pridat kategorie se stejnym jmenem
//    TODO nemelo by jit udelat z hlavni kategorie podkategorie kdyz sama ma podkategorie
	public function productProcess($form)
	{
        $val = $form->getValues();

        $navigation = $this->navigation;
        $navigation->bind($val);
        $navigation->slug = Strings::webalize($val['name']);
        if($navigation->id == NULL)
            $navigation->sort = Create::Navigation()->countByParentId($val['parent_id']) + 1;
        if ($navigation->marginLeft == 0 && $navigation->size == 0) {
            $navigation->marginLeft = 0;
            $navigation->size = 100;
        }
        $navigation->save();
        $this->presenter->redirect('this');
	}




	public function render()
	{
		$this->template->setFile(__DIR__.'/Form.latte');
		$this->template->render();
	}
}
