<?php

namespace AdminModule;

use Nette;
use Nette\Application\UI\Form;
use Nette\Application\UI\Control;
use Nette\Utils\Strings;
use Bean\ORM\Create;

class UserForm extends Control
{

    private $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

	public function createComponentForm()
	{

		$form = new Form();
        $form->addText('firstName', 'Jméno')->setValue($this->user->firstName);
        $form->addText('lastName', 'Přijmení')->setValue($this->user->lastName);
        $form->addText('email', 'E-mail')->setValue($this->user->email);
        $form->addSelect('role', 'Role', array('user' => 'Uživatel', 'admin' => 'Administrátor' ))->setValue($this->user->role);
        $form->addSelect('enabled', 'Stav účtu', array('0' => 'Zablokován', '1' => 'Povolen' ))->setValue($this->user->id === null ? 1 : $this->user->enabled);
        if($this->user->id)
            $form->addCheckbox('changePassword', 'Změnit heslo');
        $form->addPassword('password', 'Heslo');
        $form->addPassword('passwordVerification', 'Ověření hesla');
        $form->addSubmit('send', 'Uložit');
        $form->onSuccess[] = $this->userProcess;
		return $form;
	}



	public function userProcess($form)
	{
        $val = $form->getValues();

        $user = Create::User();
        if($this->user->id)
            $user->findByEmailAndId($val['email'], array('!=', $this->user->id));
        else
            $user->findByEmail($val['email']);

        if($this->user->id === null || $val['changePassword']) {
            if($val['firstName'] != "" && $val['lastName'] != "" && $val['email'] != "" && $val['password'] != "" && $val['password'] == $val['passwordVerification'] && $user->id === NULL) {
                unset($val['passwordVerification']);
                unset($val['changePassword']);
                $this->user->bind($val);
                $this->user->password = \Model\HashPassword::hash($this->user->password);
                $this->user->save();
                $this->presenter->redirect('User:default');
            }
        } else if(!$val['changePassword']) {
            if($val['firstName'] != "" && $val['lastName'] != "" && $val['email'] != "" && $user->id === NULL) {
                unset($val['passwordVerification']);
                unset($val['password']);
                unset($val['changePassword']);
                $this->user->bind($val);
                $this->user->save();
                $this->presenter->redirect('User:default');
            }
        }

        if ($val['firstName'] == "")
            $form['firstName']->addError("Jméno musí být vyplněno");
        if ($val['lastName'] == "")
            $form['lastName']->addError("Příjmení musí být vyplněno");
        if ($val['password'] == "")
            $form['password']->addError("Heslo musí být vyplněn");
        else if($val['password'] != $val['passwordVerification'])
            $form['password']->addError("Hesla se musí shodovat");
        if ($val['email'] == "")
            $form['email']->addError("Email musí být vyplněn");
        else if($user->id !== NULL)
            $form['email']->addError("Tento email již exituje");


	}




	public function render()
	{
		$this->template->setFile(__DIR__.'/Form.latte');
		$this->template->render();
	}
}
