<?php

namespace AdminModule;

use Nette;
use Nette\Application\UI\Form;
use Nette\Application\UI\Control;
use Nette\Utils\Strings;
use Bean\ORM\Create;

class ProductForm extends Control
{
    private $product;

	public function __construct($product = null)
	{
        $this->product = $product;
	}


	public function createComponentForm()
	{
        $navigation = $this->prepareNavigation(Create::Navigation()->findAll());

		$form = new Form();
        $form->addText('name', 'Název')->setValue($this->product->name);
        $form->addText('price', 'Cena')->setValue($this->product->price);
        $form->addSelect('categoryId', 'Kategorie', $navigation)->setValue($this->product->categoryId);
        $form->addText('stock', 'Skladem')->setValue($this->product->stock);
        $form->addTextArea('description', 'Popis')->setValue($this->product->description);
        $form->addUpload('img', 'Soubory', TRUE);
        // $form->addCheckbox('orderWhenStockOut', 'Umožnit objednávky i když není zboží na skladě');
        // $form->addText('stockOutMessage', 'Zpráva')->setValue($this->product->stock);
        $form->addText('keywords', 'Klíčová slova')->setValue($this->product->keywords);
		$form->addSubmit('send', 'Uložit');
		$form->onSuccess[] = $this->productProcess;
		return $form;
	}



	public function productProcess($form)
	{
        $val = $form->getValues();
        $files = $val['img'];
        unset($val['img']);


        $product = $this->product;
        $product->bind($val);
        if($product->status == NULL)
            $product->status = "notselling";
        $product->slug = Strings::webalize($product->name);
        $product->save();

        $time = time();
        for($i = 0; $i < count($files); $i++ ) {
            $fileEnd = Strings::match($files[$i]->name, '~\.jpg$|\.jpeg$|\.png$|\.gif$~i');
            $fileEnd = $fileEnd[0];
            $productImage = create::ProductImage();
            $productImage->slug = $time."_".$i.$fileEnd;
            $productImage->productId = $product->id;
            $i == 0 ? $productImage->title = 1 : $productImage->title = 0;
            $productImage->save();
            $files[$i]->move('upload/products/'.$product->slug.'/'.$productImage->slug);
        }

	}


    private function prepareNavigation($navigation)
    {
        $finalNavigation = array();

        foreach($navigation as $item) {
            if ($item->parentId === NULL) {
                $finalNavigation[$item->id] = array('parent' => $item);
            }
        }


        foreach($navigation as $item) {
            if ($item->parentId != NULL) {
                $finalNavigation[$item->parentId][$item->id] = $item;
            }
        }

        $sortedNavigation = array();
        foreach($finalNavigation as $item) {
            foreach($item as $row) {
                $sortedNavigation[$row->id] = $row->name;
            }
        }

        return $sortedNavigation;
    }


	public function render()
	{
		$this->template->setFile(__DIR__.'/Form.latte');
		$this->template->render();
	}
}
