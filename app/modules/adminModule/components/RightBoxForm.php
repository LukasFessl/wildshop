<?php

namespace AdminModule;

use Nette;
use Nette\Application\UI\Form;
use Nette\Application\UI\Control;
use Nette\Utils\Strings;
use Bean\ORM\Create;

class RightBoxForm extends Control
{
    private $rightBox;

    public function __construct($rightBox = null)
    {
        $this->rightBox = $rightBox;
    }


    public function createComponentForm()
    {
        $form = new Form();
        $form->addText('name', 'Název')->setValue($this->rightBox->name);
        $form->addCheckbox('name_allowed', 'Zobrazovat názeb boxu')->setValue($this->rightBox->id == NULL ? "1" : $this->rightBox->nameAllowed);
        $form->addSelect('type', 'Typ', array('classic' => 'classic', 'discount'=>'sleva'))->setValue($this->rightBox->type);
        $form->addTextArea('text', 'Popis')->setValue($this->rightBox->text);
        $form->addSubmit('send', 'Uložit');
        $form->onSuccess[] = $this->rightBoxProcess;
        return $form;
    }



    public function rightBoxProcess($form)
    {
        $val = $form->getValues();

        if ($val['name'] != "") {
            $rightBox = $this->rightBox;
            $rightBox->bind($val);
            if ($rightBox->id == NULL) {
                $rightBoxCount = Create::RightBox()->count();
                $rightBox->sort = $rightBoxCount + 1;
            }
            $rightBox->save();
        } else {
            $val['name']->addError("Jméno musí být vyplněno");
        }

    }


    public function render()
    {
        $this->template->setFile(__DIR__.'/Form.latte');
        $this->template->render();
    }
}
