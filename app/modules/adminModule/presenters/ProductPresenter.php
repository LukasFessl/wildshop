<?php

namespace AdminModule;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Bean\ORM\Create;

class ProductPresenter extends BasePresenter
{

    public function startup()
    {
        parent::startup();
        if(!$this->user->isLoggedIn()) {
            $this->redirect("auth:default");
        }
        else if($this->user->isLoggedIn()) {
            if($this->user->getIdentity()->role == "admin" && $this->user->getIdentity()->enabled == 1) {
                if($this->name == "Admin:Auth") {
                    $this->redirect("auth:default");
                }
            }
        }
    }

    public function renderDefault()
    {
        $this->template->products = Create::Product()->findAllOrder("id DESC");
        $this->template->productImages = Create::ProductImage()->setIndex('product_id')->findAllByTitle(1);
    }


    public function getImgPath($product, $productImages)
    {
        if (isset($productImages[$product->id]))
            return "/upload/products/".$product->slug."/".$productImages[$product->id]->slug;

        return "none.png";
    }


    public function actionAdd() {

    }


    public function createComponentProductForm()
	{
        $product = create::product()->get($this->getParameter('id'));
		return new ProductForm($product);
	}


    public function actionEdit($id = NULL) {

    }


    public function handleSelling($id = NULL) {
        $product = create::product()->get($id);
        $product->status = "selling";
        $product->save();
    }

    public function handleNotSelling($id = NULL) {
        $product = create::product()->get($id);
        $product->status = "notselling";
        $product->save();
    }

    public function handleDelete($id = NULL) {
        $product = create::product()->get($id);
        $product->delete();
    }



}
