<?php

namespace AdminModule;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Bean\ORM\Create;

class RightBoxPresenter extends BasePresenter
{

    public function startup()
    {
        parent::startup();
        if(!$this->user->isLoggedIn()) {
            $this->redirect("auth:default");
        }
        else if($this->user->isLoggedIn()) {
            if($this->user->getIdentity()->role == "admin" && $this->user->getIdentity()->enabled == 1) {
                if($this->name == "Admin:Auth") {
                    $this->redirect("auth:default");
                }
            }
        }
    }



    public function renderDefault()
    {
        $rightBoxes = Create::RightBox()->findAllOrder('sort ASC');
        $this->template->rightBoxes = $rightBoxes;
    }

    public function createComponentRightBoxForm()
    {
        $rightBox = Create::RightBox()->get($this->getParameter('id'));
        return new RightBoxForm($rightBox);
    }

    public function renderEdit($id)
    {

    }

    public function handleRightBoxDelete($id)
    {
        if ($this->isAjax()) {
            $rightBoxes = Create::RightBox()->findAllOrder('sort ASC');
            $i = 1;
            foreach ($rightBoxes as $rightBox) {
                if ($rightBox->id == $id) {
                    $rightBox->delete($id);
                } else {
                    $rightBox->sort = $i;
                    $rightBox->save();
                    $i++;
                }
            }
            $this->invalidateControl("RightBoxList");
        }
    }

    public function handleRightBoxMove($direction, $id)
    {
        if ($this->isAjax()) {
            if ($direction == 'top') {
                $rightBox = Create::RightBox()->findById($id);
                if ($rightBox->sort > 1) {
                    $rightBox2 = Create::RightBox()->findBySort($rightBox->sort - 1);
                    $rightBox->sort = $rightBox->sort - 1;
                    $rightBox2->sort = $rightBox2->sort + 1;
                    $rightBox->save();
                    $rightBox2->save();
                }
            } else if ($direction == 'bottom') {
                $rightBox = Create::RightBox()->findById($id);
                $rightBox2 = Create::RightBox()->findBySort($rightBox->sort + 1);
                if ($rightBox2->id != NULL) {
                    $rightBox->sort = $rightBox->sort + 1;
                    $rightBox2->sort = $rightBox2->sort - 1;
                    $rightBox->save();
                    $rightBox2->save();
                }
            }
            $this->invalidateControl('RightBoxList');
        }
    }


}
