<?php

namespace AdminModule;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Bean\ORM\Create;

class UserPresenter extends BasePresenter
{

    public function startup()
    {
        parent::startup();
        if(!$this->user->isLoggedIn()) {
            $this->redirect("auth:default");
        }
        else if($this->user->isLoggedIn()) {
            if($this->user->getIdentity()->role == "admin" && $this->user->getIdentity()->enabled == 1) {
                if($this->name == "Admin:Auth") {
                    $this->redirect("auth:default");
                }
            }
        }
    }

    public function actionDefault()
    {
        $this->template->users = create::user()->findAll();
    }


    public function createComponentUserForm()
    {
        $user = create::user()->get($this->getParameter('id'));
        return new UserForm($user);
    }


    public function actionEdit($id)
    {

    }

}
