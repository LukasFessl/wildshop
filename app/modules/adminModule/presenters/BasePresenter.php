<?php

namespace AdminModule;

use Bean\ORM\BormSettings;
use Nette;
use Bean\ORM\Create;



class BasePresenter extends Nette\Application\UI\Presenter
{
	public function startup()
	{
		parent::startup();
		if($this->action == "logout" && $this->name == "Admin:Auth") {
			$this->logout();
		}
		BormSettings::set($this->context->bean);
	}


	public function beforeRender()
	{
		$menu = array(	'Hlavní strana' => array('title' => 'Hlavní strana', 'link' => 'board:default', 'presenter' => 'Board:*', 'i' => 'fa fa-tachometer'),
						'Produkty' => array('title' => 'Produkty', 'link' => 'Product:default', 'presenter' => 'Product:*', 'i' => 'fa fa-tachometer',),
						'Nastavení' => array('title' => 'Nastavení', 'link' => 'Logo:default', 'presenter' => 'Logo:*', 'i' => 'fa fa-wrench',
                            'submenu' => array('presenters' => array('Admin:Settings:*','Admin:Shipping:*','Admin:Slider:*','Admin:RightBox:*','Admin:Logo:*','Admin:Navigation:*'),
                                                'pages' => array(
                                                    'Slider' => array('title' => 'Slider', 'link' => 'Slider:default'),
                                                    'Logo' => array('title' => 'Logo', 'link' => 'Logo:default'),
                                                    'Navigace' => array('title' => 'Navigace', 'link' => 'Navigation:default'),
                                                    'Postraní panel' => array('title' => 'Postraní panel', 'link' => 'RightBox:default'),
                                                    'Doprava' => array('title' => 'Doprava', 'link' => 'Shipping:default'))
                                                    )),
						'Uživatelé' => array('title' => 'Uživatelé', 'link' => 'User:default', 'presenter' => 'User:*', 'i' => 'fa fa-user'),
					);

		$this->template->menu = $menu;
		$this->template->currentUser = $this->user->getIdentity();
	}
}
