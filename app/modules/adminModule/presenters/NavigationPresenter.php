<?php

namespace AdminModule;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Bean\ORM\Create;

class NavigationPresenter extends BasePresenter
{

    public function startup()
    {
        parent::startup();
        if(!$this->user->isLoggedIn()) {
            $this->redirect("auth:default");
        }
        else if($this->user->isLoggedIn()) {
            if($this->user->getIdentity()->role == "admin" && $this->user->getIdentity()->enabled == 1) {
                if($this->name == "Admin:Auth") {
                    $this->redirect("auth:default");
                }
            }
        }
    }



    public function renderDefault()
    {
        $navigation = create::navigation()->findAllOrder('sort ASC');;
        $this->template->navigation = $this->prepareNavigation($navigation);
    }


    private function prepareNavigation($navigation)
    {
        $finalNavigation = array();

        foreach ($navigation as $item) {
            if ($item->parentId === NULL) {
                $finalNavigation[$item->id] = array('parent' => $item);
            }
        }


        foreach ($navigation as $item) {
            if ($item->parentId != NULL) {
                $finalNavigation[$item->parentId][$item->id] = $item;
            }
        }
        return $finalNavigation;
    }


    public function createComponentNavigationForm()
    {
        $navigation = Create::Navigation()->get($this->getParameter('id'));
        return new NavigationForm($navigation);
    }


    public function actionEdit($id)
    {

    }


    public function handleNavigationMove($direction, $navId, $subnavId = NULL)
    {
        if ($this->isAjax()) {
            if ($direction == 'top') {
                $nav = Create::Navigation()->get($navId);
                $nav2 = Create::Navigation()->findBySortAndParentId($nav->sort - 1, $subnavId);
                if ($nav->sort > 1) {
                    $nav->sort = $nav->sort - 1;
                    $nav->save();
                    $nav2->sort = $nav2->sort + 1;
                    $nav2->save();
                }
            } else if ($direction == 'bottom') {
                $nav = Create::Navigation()->get($navId);
                $nav2 = Create::Navigation()->findBySortAndParentId($nav->sort + 1, $subnavId);
                if ($nav2->id != NULL) {
                    $nav->sort = $nav->sort + 1;
                    $nav->save();
                    $nav2->sort = $nav2->sort - 1;
                    $nav2->save();
                }
            }
            $this->invalidateControl('navigationList');
        }
    }


    public function handleNavigationDelete($id, $parentId = NULL)
    {
        $navChildCount = Create::Navigation()->countByParentId($id);

        if ($navChildCount == 0) {
            $countProducts = Create::Product()->countByCategoryId($id);
            if ($countProducts == 0) {
                $navigation = Create::Navigation()->findAllByParentIdOrder($parentId, 'sort ASC');
                $i = 1;
                foreach ($navigation as $nav) {
                    if ($nav->id == $id) {
                        $nav->delete($id);
                    } else {
                        $nav->sort = $i;
                        $nav->save();
                        $i++;
                    }
                }

            } else {
                $this->flashMessage('Tuto záložku nelze odstranit protože obsahuje produkty', 'error');
            }
        } else {
            $this->flashMessage('Tuto záložku nelze odstranit protože obsahuje podzáložky', 'error');
        }
    }





}
