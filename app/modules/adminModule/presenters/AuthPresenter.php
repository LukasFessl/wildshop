<?php

namespace AdminModule;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Bean\ORM\Create;

class AuthPresenter extends BasePresenter
{
    public function startup()
    {
        parent::startup();
        if($this->user->isLoggedIn()) {
            if($this->user->getIdentity()->role == "admin" && $this->user->getIdentity()->enabled == 1) {
                $this->redirect("board:default");
            }
        }
    }

    public function actionDefault()
    {
        $this->setLayout('authLayout');
        $user = Create::User()->count();
        if(!$user) {
            $user = Create::User();
            $user->email = "test";
            $user->password = \Model\HashPassword::hash("test");
            $user->enabled = 1;
            $user->role = "admin";
            $user->save();
        }

    }


    public function createComponentAuthForm()
    {
        $form = new Form();
        $form->addText('email', 'Email');
        $form->addPassword('password', 'Heslo');
        $form->addSubmit('login', 'Přihlásit');
        $form->onSuccess[] = $this->authProcess;

        return $form;
    }



    public function authProcess($form)
    {
        $authenticator = $this->context->authenticator;
        $val = $form->getValues();

        $user = $this->getUser();
        $user->setAuthenticator($authenticator);

        try {
            $user->login($val->email, $val->password, 'admin');
            $user->setExpiration('120 minutes', FALSE);
            $this->redirect("board:default");
        } catch (AuthenticationException $e) {
            if($e->getCode() == IAuthenticator::INVALID_CREDENTIAL)
                $form->addError("Nesprávné jméno nebo heslo");
            else
                $form->addError("Nastala neznámá chyba !!!");
        }

    }




    protected function logout()
    {
        $user = $this->getUser();
        $user->logout(TRUE);
        $this->redirect('auth:default');
    }

}
