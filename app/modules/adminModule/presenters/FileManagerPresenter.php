<?php

namespace AdminModule;

use Model\Entity\Image;
use Nette;
use Nette\Application\UI\Form;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Bean\ORM\Create;

class FileManagerPresenter extends BasePresenter
{
    public function startup()
    {
        parent::startup();
        if(!$this->user->isLoggedIn()) {
            $this->redirect("auth:default");
        }
        else if($this->user->isLoggedIn()) {
            if($this->user->getIdentity()->role != "admin" || $this->user->getIdentity()->enabled != 1) {
                if($this->name == "Admin:Auth") {
                    $this->redirect("auth:default");
                }
            }
        }
    }

    public function actionDefault() {
        $this->setLayout("fileManagerLayout");
        $this->template->files = Create::Image()->findAllByType(Image::FILE);
    }



    public function createComponentFileUploadForm()
    {
        $form = new Form();
        $form->addUpload('file', 'Soubor');
        $form->addSubmit('send', 'Uložit');
        $form->onSuccess[] = $this->fileProcess;
        return $form;
    }


    public function fileProcess($form)
    {
        $val = $form->getValues();

//        $fileEnd = Strings::match($val['file']->name, '~\.jpg$|\.jpeg$|\.png$|\.gif$~i');
//        $fileEnd = $fileEnd[0];

        if ($val['file']->isOk()) {
            $time = time();
            $image = Create::Image();
            $image->name = $time.$val['file']->name;
            $image->slug = $time.$val['file']->name;
            $image->type = Image::FILE;
            $image->sort = 0;

            if($image->save()) {
                $val['file']->move('upload/images/'.$image->slug);
                $this->presenter->flashMessage("Soubor byl vložen");
                $this->presenter->redirect('this');
            }
        } else {
            if(!$val['file']->isOk())
                $form->addError('Při nahrávání souboru došlo k neznámé chybě');
        }
    }


}
