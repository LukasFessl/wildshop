<?php

namespace AdminModule;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Bean\ORM\Create;

class LogoPresenter extends BasePresenter
{

    public function startup()
    {
        parent::startup();
        if(!$this->user->isLoggedIn()) {
            $this->redirect("auth:default");
        }
        else if($this->user->isLoggedIn()) {
            if($this->user->getIdentity()->role == "admin" && $this->user->getIdentity()->enabled == 1) {
                if($this->name == "Admin:Auth") {
                    $this->redirect("auth:default");
                }
            }
        }
    }


    public function renderDefault()
    {
        $config = Create::Config()->setIndex('name')->findAll();
        $this->template->logo = isset($config['logo']) ? $config['logo'] : NULL;
        $this->template->favIcon = isset($config['favicon']) ? $config['favicon'] : NULL;
    }

    public function createComponentLogoForm()
    {
        return new ImageUploadForm('logo');
    }

    public function createComponentFavIconForm()
    {
        return new ImageUploadForm('favicon');
    }

    public function createComponentWebNameForm()
    {
        return new TextForm('web-name');
    }




}
