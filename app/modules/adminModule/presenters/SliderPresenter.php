<?php

namespace AdminModule;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Bean\ORM\Create;
use Model\Entity\Image;

class SliderPresenter extends BasePresenter
{

    public function startup()
    {
        parent::startup();
        if(!$this->user->isLoggedIn()) {
            $this->redirect("auth:default");
        }
        else if($this->user->isLoggedIn()) {
            if($this->user->getIdentity()->role == "admin" && $this->user->getIdentity()->enabled == 1) {
                if($this->name == "Admin:Auth") {
                    $this->redirect("auth:default");
                }
            }
        }
    }

    public function renderDefault()
    {
        $this->template->imagesSelected = Create::Image()->findAllBySortAndTypeOrder(array('>', 0), 'slider', 'sort ASC');
        $this->template->imagesUnselected = Create::Image()->findAllBySortAndType(0, 'slider');
    }


    public function createComponentSliderForm()
    {
        return new ImageUploadForm(Image::SLIDER);
    }


    public function handleSliderChangeState($id)
    {
        if ($this->isAjax()) {
            $image = Create::Image()->get($id);
            if ($image->sort > 0) {
                $image->sort = 0;
                $image->save();

                $images = Create::Image()->findAllBySortAndTypeOrder(array('>', 0), 'slider', 'sort ASC');

                for ($i = 0; $i < count($images); $i++) {
                    $images[$i]->sort = $i + 1;
                    $images[$i]->save();
                }
            } else if ($image->sort == 0) {
                $count = Create::Image()->countBySortAndType(array('>', 0), 'slider');
                $image->sort = $count + 1;
                $image->save();
            }
            $this->invalidateControl("selectImageList");
            $this->invalidateControl("unselectImageList");
        }
    }


    public function handleSliderMove($direction, $id)
    {
        if ($this->isAjax()) {
            $image = Create::Image()->get($id);
            if ($direction === 'right') {
                $count = Create::Image()->countBySortAndType(array('>', 0), 'slider');
                if ($image->sort < $count) {
                    $image2 = Create::Image()->findBySortAndType($image->sort + 1, 'slider');
                    $image->sort = $image->sort + 1;
                    $image2->sort = $image2->sort - 1;
                    $image->save();
                    $image2->save();
                }
            } else if ($direction === 'left') {
                if ($image->sort > 1) {
                    $image2 = Create::Image()->findBySortAndType($image->sort - 1, 'slider');
                    $image->sort = $image->sort - 1;
                    $image2->sort = $image2->sort + 1;
                    $image->save();
                    $image2->save();
                }
            }
            $this->invalidateControl("selectImageList");
        }
    }


    public function handleDeleteSlider($imageId) {

        if ($this->isAjax()) {
            $image = Create::Image()->get($imageId);

            if (file_exists('upload/images/' . $image->slug)) {
                unlink('upload/images/' . $image->slug);
            }

            $image->delete();
            $this->invalidateControl('unselectImageList');
        }
    }



}
