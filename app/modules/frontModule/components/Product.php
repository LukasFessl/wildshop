<?php

namespace FrontModule;

use Nette;
use Nette\Application\UI\Form;
use Nette\Application\UI\Control;
use Nette\Utils\Strings;
use Bean\ORM\Create;

class Product extends Control
{

    private $product;

    public function __construct($product)
    {
        $this->product = $product;
    }


    protected function isLoggedInUser()
    {
        if($this->presenter->user->isLoggedIn()) {
            if($this->presenter->user->getIdentity()->role == 'user')
                return true;
        }

        return false;
    }


	public function render()
	{
		$this->template->setFile(__DIR__.'/Product.latte');
        $this->template->product = $this->product;
        $this->template->currentUser = $this->isLoggedInUser() ? $this->presenter->user->getIdentity() : NULL;
		$this->template->render();
	}
}
