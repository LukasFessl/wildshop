<?php

namespace FrontModule;

use Nette;
use Nette\Application\UI\Form;
use Nette\Application\UI\Control;
use Nette\Utils\Strings;
use Bean\ORM\Create;

class RightBox extends Control
{

    private $rightBox;

    public function __construct($rightBox)
    {
        $this->rightBox = $rightBox;
    }





    public function render()
    {
        $this->template->setFile(__DIR__.'/RightBox.latte');
        $this->template->rightBox = $this->rightBox;
        $this->template->render();
    }
}
