<?php

namespace FrontModule;

use Model\Entity\CartOrder;
use Nette;
use Nette\Application\UI\Form;
use Nette\Application\UI\Control;
use Nette\Utils\Strings;
use Bean\ORM\Create;

class Cart extends Control
{

    private $cartContent;


    private function generateMessage()
    {
        $cartSession = $this->presenter->getSession('cart');
        $cartMessage = "Košík je prázdný";

        $cartSessionArray = null;
        if (!$this->isLoggedInUser()) {
            $cartSessionArray = array();
            $i = 0;
            foreach ($cartSession as $c) {
                $tempCart = Create::Cart();
                $tempCart->productId = $c['id'];
                $tempCart->quantity = $c['quantity'];
                array_push($cartSessionArray, $tempCart);
                $i++;
            }
        }

        // if cartSessionArray == null load cart from db; transform data
        $cartContent = $this->getCart($cartSessionArray);
        $this->cartContent = $cartContent;
        $count = 0;
        $price = 0;
        foreach ($cartContent as $cart) {
            $price += $cart->quantity * $cart->productId->price;
            $count += $cart->quantity;

            $cartMessage = $count . " ks za " . $price . " kč";
        }

        return $cartMessage;

    }


    private function getCart($cart = null)
    {
        if (!is_array($cart))
            $cart = Create::Cart()->findAllByUserId($this->presenter->user->getIdentity()->id);

        //find all products in cart
        $query = "";
        for ($i = 0; $i < count($cart); $i++) {
            $query .= $cart[$i]->productId . "";
            if ($i < count($cart) - 1)
                $query .= "' OR id = '";
        }
        $products = Create::Product()->findAllById($query);

        //put to gether cart and product
        for ($i = 0; $i < count($cart); $i++) {
            $cart[$i]->productId;

            for ($j = 0; $j < count($products); $j++) {

                if ($cart[$i]->productId == $products[$j]->id) {
                    $cart[$i]->productId = $products[$j];
                    break;
                }

            }
        }

        return $cart;
    }


    public function handleRemoveProduct($productId)
    {
        if ($this->presenter->isAjax()) {
            if ($this->isLoggedInUser()) {
                $user = $this->presenter->user->getIdentity();
                $cart = Create::Cart()->findAllByProductIdAndUserId($productId, $user->id);
                foreach ($cart as $item) {
                    $item->delete();
                }

                $cartCount = Create::Cart()->countByUserId($user->id);
                if ($cartCount == 0) {
                    Create::CartOrder()->delete($cart[0]->cartOrderId);
                }
            } else {
                $cartSession = $this->presenter->getSession('cart');
                foreach ($cartSession as $key => $value) {
                    if ($value['id'] == $productId) {
                        unset($cartSession->$key);
                    }
                }
            }
            $this->presenter->invalidateControl("cartTop");
        }
    }



    protected function isLoggedInUser()
    {
        if($this->presenter->user->isLoggedIn()) {
            if($this->presenter->user->getIdentity()->role == 'user')
                return true;
        }

        return false;
    }


	public function render()
	{
		$this->template->setFile(__DIR__.'/Cart.latte');
        $this->template->cartMessage = $this->generateMessage();
        $this->template->products = $this->cartContent;
        $this->template->cartOrderCart = CartOrder::CART;
		$this->template->render();
	}
}
