<?php

namespace FrontModule;

use Nette;
use Nette\Application\UI\Form;
use Nette\Application\UI\Control;
use Nette\Utils\Strings;
use Bean\ORM\Create;

class UserForm extends Control
{

	public function createComponentForm()
	{
		$form = new Form();
        $form->addText('firstName', 'Jméno');
        $form->addText('lastName', 'Přijmení');
        $form->addText('email', 'E-mail');
        $form->addPassword('password', 'Heslo');
        $form->addPassword('passwordVerification', 'Ověření hesla');

        $form->addText('country', 'Země');
        $form->addText('city', 'Město');
        $form->addText('address', 'Adresa');
        $form->addText('zip', 'Směrovací číslo');

        $form->addSubmit('send', 'Uložit');
        $form->onSuccess[] = $this->userProcess;
		return $form;
	}



	public function userProcess($form)
	{
        $val = $form->getValues();
        // dump($val['firstName']);
        // exit();

        if($val['firstName'] == "")
            $form['firstName']->addError("Jméno musí být vyplněno");
        if($val['lastName'] == "")
            $form['lastName']->addError("Příjmení musí být vyplněno");
        if($val['email'] == "")
            $form['email']->addError("Email musý být vyplněn");


        $user = create::user()->findByEmail($val['email']);
        if($user->id === NULL) {
            if($val['password'] != "" && $val['password'] == $val['passwordVerification']) {
                unset($val['passwordVerification']);
                $user->bind($val);
                $user->enabled = 1;
                $user->role = 'user';
                $user->password = \Model\HashPassword::hash($user->password);
                $user->save();
            }
        } else {
            $form['email']->addError("Tento email již exituje");
        }

	}




	public function render()
	{
		$this->template->setFile(__DIR__.'/Form.latte');
		$this->template->render();
	}
}
