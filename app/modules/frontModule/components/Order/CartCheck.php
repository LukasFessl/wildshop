<?php

namespace FrontModule;

use Model\Entity\CartOrder;
use Nette;
use Nette\Application\UI\Form;
use Nette\Application\UI\Control;
use Bean\ORM\Create;

class CartCheck extends Control
{

    public function createComponentForm()
    {
        $form = new Form();
        $form->addSubmit('send', 'Pokračovat');
        $form->onSuccess[] = $this->process;
        return $form;
    }



    public function process($form)
    {
        $val = $form->getValues();
        $cart = Create::Cart()->findByUserId($this->presenter->user->id);
        $cartOrder = Create::CartOrder()->get($cart->cartOrderId);
        $cartOrder->state = CartOrder::ADDRESS;
        $cartOrder->save();
        $this->presenter->redirect('Order:default', array('step' => 'address'));

    }

    private function cartCheck()
    {
        $user = $this->presenter->user->getIdentity();

        $cart = Create::Cart()->findAllByUserId($user->id);

        $query = "";
        for ($i = 0; $i < count($cart); $i++) {
            $query .= $cart[$i]->productId."";

            if($i < count($cart)-1)
                $query .= "'OR id = '";
        }
        $products = Create::Product()->setIndex('id')->findAllById($query);

        $query = "";
        for ($i = 0; $i < count($cart); $i++) {
            $query .= $cart[$i]->productId."";

            if($i < count($cart)-1)
                $query .= "'OR id = '";
        }
        $productImages = Create::ProductImage()->setIndex('product_id')->findAllByProductIdAndTitle($query, 1);

        $this->template->cartItems = $cart;
        $this->template->products = $products;
        $this->template->productImages = $productImages;
    }


    public function render()
    {
        $this->template->setFile(__DIR__.'/CartCheck.latte');
        $this->cartCheck();
        $this->template->render();
    }
}
