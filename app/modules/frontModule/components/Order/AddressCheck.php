<?php

namespace FrontModule;

use Model\Entity\CartOrder;
use Nette;
use Nette\Application\UI\Form;
use Nette\Application\UI\Control;
use Bean\ORM\Create;

class AddressCheck extends Control
{

    public function createComponentForm()
    {
        $user = Create::User()->get($this->presenter->user->getIdentity()->id);
        $form = new Form();
        $form->addText('deliveryCountry', 'Země')->setValue(!$user->country ? "Česká republika" : $user->country);
        $form->addText('deliveryCity', 'City:')->setValue($user->city);
        $form->addText('deliveryAddress', 'Address:')->setValue($user->address);
        $form->addText('deliveryZip', 'Sm. číslo:')->setValue($user->zip);
        $form->addCheckbox('billing','Fakturační adresa je stejná jako doručovací')->setValue(1);
        $form->addText('billingCountry', 'Země');
        $form->addText('billingCity', 'City:');
        $form->addText('billingAddress', 'Address:');
        $form->addText('billingZip', 'Sm. číslo:');
        $form->addSubmit('send', 'Pokračovat');
        $form->onSuccess[] = $this->process;
        return $form;
    }



    public function process($form)
    {
        $val = $form->getValues();
        $cart = Create::Cart()->findByUserId($this->presenter->user->id);
        $cartOrder = Create::CartOrder()->get($cart->cartOrderId);
        $cartOrder->state = CartOrder::SHIPPING;

        $billing = $val['billing'];
        unset($val['billing']);
        $cartOrder->bind($val);

        if ($billing) {
            $cartOrder->billingCountry = $val['deliveryCountry'];
            $cartOrder->billingCity = $val['deliveryCity'];
            $cartOrder->billingAddress = $val['deliveryAddress'];
            $cartOrder->billingZip= $val['deliveryZip'];
        }

        $cartOrder->save();
        $this->presenter->redirect('Order:default', array('step' => 'shipping'));

    }


    public function render()
    {
        $this->template->setFile(__DIR__.'/AddressCheck.latte');
        $this->template->render();
    }
}
