<?php

namespace FrontModule;

use Bean\ORM\BormSettings;
use Bean\ORM\OrmSettings;
use Nette;
use Nette\Utils\Strings;
use Bean\ORM\Create;



class BasePresenter extends Nette\Application\UI\Presenter
{


	public function startup()
	{
		parent::startup();
        BormSettings::set($this->getContext()->getService('bean'));
	}


	public function beforeRender()
	{
//		 foreach ($this->cartSession as $key => $value) {
//		 	unset($this->cartSession->$key);
//		 }

		$navigation = Create::Navigation()->findAllOrder('sort ASC');
		$this->template->navigation = $navigation;
		$config = Create::Config()->setIndex('name')->findAll('logo');

        $this->template->favIcon = isset($config['favicon']) ? '/upload/images/'.$config['favicon']->value : NULL;
        $this->template->logoPath = !isset($config['logo']) ? '/upload/images/logoNone.png' : '/upload/images/'.$config['logo']->value;
        $this->template->webName = isset($config['web-name']) ? '/upload/images/'.$config['web-name']->value : NULL;
		$this->template->currentPage = $this->getParameter('page');

		$this->template->currentUser = NULL;
		if ($this->isLoggedInUser()) {
			$this->template->currentUser = $this->user->getIdentity();
		}


        $rightBoxes = Create::RightBox()->findAllBySort(array('>', 0));
        foreach ($rightBoxes as $rightBox) {
            $this['rightBox'.$rightBox->id] = new RightBox($rightBox);
        }
        $this->template->rightBoxes = $rightBoxes;


	}



	public function isLoggedInUser()
	{
		if($this->user->isLoggedIn()) {
			if($this->user->getIdentity()->role == 'user')
				return true;
		}

		return false;
	}



	public function getSubnavigation($navigation, $currentItemFromNavigation)
	{
		$subnav = array();
		foreach($navigation as $item) {
			if($item->parentId == $currentItemFromNavigation->id) {
				array_push($subnav, $item);
			}
		}

		return $subnav;
	}



	public function createComponentCartInfo()
	{
		return new Cart();
	}



    public function handleLogout()
    {
        $user = $this->getUser();
        $user->logout(TRUE);
        $this->redirect('this');
    }


	public function buildQuery($object, $expression, $caompareVar)
    {
        $query = "";
        for ($i = 0; $i < count($object); $i++) {
              $query .= $object[$i]->id."";

            if($i < count($object)-1)
                $query .= "'".$expression." ".$caompareVar." = '";
        }
    dump($query);
        return $query;
    }
}
