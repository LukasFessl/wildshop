<?php

namespace FrontModule;

use Nette;
use Bean\ORM\Create;



class HomepagePresenter extends BasePresenter
{

    private $productImages;

    protected $cartSession;



    public function actionDefault($page, $subpage)
    {
        $this->cartSession = $this->getSession('cart');

    }



    public function renderDefault($page, $subpage)
    {
        $page = Create::Navigation()->findBySlug($this->getParameter('page'));

        $currentPageType = $page->type;
        if($this->getParameter('page') == "" && is_null($this->getParameter('productSlug'))) {
            $this->template->images = Create::Image()->findAllBySortAndType(array('>', 0), 'slider');
            $currentPageType = 'home';
        }
        $this->template->currentPageType = $currentPageType;




        $subPage = Create::Navigation()->findBySlugAndParentId($this->getParameter('subpage'), $page->id);
        if ($page->type === "eshop") {
            $products = array();
            if (is_null($subPage->id)) {
                $navCatAndSubcat = Create::Navigation()->findAllByIdOrParentId($page->id, $page->id);

                $query = "";
        		for ($i = 0; $i < count($navCatAndSubcat); $i++) {
        			$query .= $navCatAndSubcat[$i]->id."";
        			if($i < count($navCatAndSubcat)-1)
        				$query .= "' OR category_id = '";
        		}
                $products = Create::Product()->findAllByCategoryIdAndStatusOrder($query, 'selling', 'id DESC');

            } else {
                $products = Create::Product()->findAllByCategoryIdAndStatusOrder($subPage->id, 'selling', 'id DESC');
            }
            foreach ($products as $product) {
                $this['product'.$product->id] = new Product($product);
            }
            $this->template->products = $products;
        }


        if($page->type == 'eshop' || $page->type == '')
            $this->productImages = Create::ProductImage()->findAllByTitle(1);
    }



    public function getTitleFotoPath($product)
    {
        $productImages = $this->productImages;

        $path = "";
        foreach ($productImages as $productImage) {
            if($productImage->productId == $product->id) {
                $path = "/upload/products/".$product->slug."/".$productImage->slug;
                break;
            }
        }
        return $path;
    }



    public function getLastProducts($count)
    {
        $products = Create::Product()->findAllByStatusOrderLimit('selling', 'id DESC', '0,'.$count);
        foreach ($products as $product) {
            $this['product'.$product->id] = new Product($product);
        }
        return $products;
    }



    public function handleAddToCart($userId, $productId)
    {
        if ($this->isAjax()) {
            if(!is_null($userId)) {
                $cartAll = Create::Cart()->findAllByUserId($userId);
                $cart = Create::Cart();

                if (count($cartAll) == 0) {
                    $cartOrder = Create::CartOrder();
                    $cartOrder->save();
                    $cart->cartOrderId = $cartOrder->id;
                } else {
                    $cart = $cart->findByProductIdAndUserId($productId, $userId);
                    $cartOrder = Create::CartOrder()->get($cartAll[0]->cartOrderId);
                    $cart->cartOrderId = $cartOrder->id;
                }
                $cart->productId = $productId;
                $cart->userId = $userId;
                $cart->quantity += 1;
                $cart->save();

            } else {
                if(!$this->cartSession[$productId])
					$this->cartSession[$productId] = array('id'=>$productId,'quantity' => 1);
				else {
					$q = $this->cartSession[$productId]['quantity'];
					$this->cartSession[$productId] = array('id'=>$productId,'quantity' => $q += 1);
				}
            }

            $this->invalidateControl('cartTop');
        }
    }



}
