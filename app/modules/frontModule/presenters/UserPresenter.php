<?php

namespace FrontModule;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Bean\ORM\Create;


class UserPresenter extends BasePresenter
{

    public function actionDefault() {

    }



    public function actionRegistration() {

    }



    public function createComponentRegistrationForm() {
        return new UserForm();
    }






    public function createComponentAuthForm()
    {
        $form = new Form();
        $form->addText('email', 'Email');
        $form->addPassword('password', 'Heslo');
        $form->addSubmit('login', 'Přihlásit');
        $form->onSuccess[] = $this->authProcess;

        return $form;
    }

    public function authProcess($form)
    {
        $authenticator = $this->context->authenticator;
        $val = $form->getValues();

        $user = $this->getUser();
        $user->setAuthenticator($authenticator);

        try {
            $user->login($val->email, $val->password, 'user');
            $user->setExpiration('120 minutes', FALSE);
            $this->moveCartFromSessionToDatabase($user);
            $this->redirect("Homepage:default");
        } catch (AuthenticationException $e) {
            if($e->getCode() == IAuthenticator::INVALID_CREDENTIAL)
                $form->addError("Nesprávné jméno nebo heslo");
            else
                $form->addError("Nastala neznámá chyba !!!");
        }
    }


	public function moveCartFromSessionToDatabase($user)
    {
        $cartSession = $this->getSession('cart');
        foreach ($cartSession as $key => $value) {
            $cart = Create::Cart()->findByProductIdAndUserId($value['id'], $user->id);
            if($cart->id == NULL) {
                $cartCount = Create::Cart()->findByUserId($user->id);
                $cartOrder = Create::CartOrder();
                if ($cartCount->id == NULL) {
                       $cartOrder->save();
                } else {
                    $cartOrder = $cartOrder->get($cartCount->cartOrderId);
                }
                $cart->userId = $user->id;
                $cart->productId = $value['id'];
                $cart->quantity = $value['quantity'];
                $cart->cartOrderId = $cartOrder->id;
            } else {
                $cart->quantity += $value['quantity'];
            }
            $cart->save();
            unset($cartSession->$key);
        }
    }


}
