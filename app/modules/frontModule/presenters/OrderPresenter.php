<?php

namespace FrontModule;

use Model\Entity\CartOrder;
use Nette;
use Bean\ORM\Create;
use Bean\ORM\Get;


class OrderPresenter extends BasePresenter
{

    public function actionDefault($step = NULL)
    {
        switch ($step) {
            case (CartOrder::CART):
                $this->template->orderState = CartOrder::CART;
                break;
            case (CartOrder::ADDRESS):
                $this->template->orderState = CartOrder::ADDRESS;
                break;
            case (CartOrder::SHIPPING):
                $this->template->orderState = CartOrder::SHIPPING;
//                $this->cartCheck();
                break;
            case (CartOrder::PAYMENT):
                $this->template->orderState = CartOrder::PAYMENT;
//                $this->cartCheck();
                break;
        }
//        $cart = Create::Cart()->findAllByUserId($user->id);
//        dump($cart);
//        $products = Get::Product($cart);
//        $products = Create::Product()->findAllById(1);
//        dump($products);
//
//
//        $productImages = Get::ProductImage($products);
//        dump($products);
//        $product = $cart->getProduct();
    }


    public function createComponentCartCheck()
    {
        return new CartCheck();
    }

    public function createComponentAddressCheck()
    {
        return new AddressCheck();
    }

//    TODO none img
    public function getImgPath($cart, $products, $productImages)
    {
        if (isset($productImages[$cart->productId])) {
            return "/upload/products/".$products[$cart->productId]->slug."/".$productImages[$cart->productId]->slug;
        }

        return "none.png";
    }

}
