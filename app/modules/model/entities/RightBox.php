<?php

namespace Model\Entity;


use Nette;
use Nette\Utils\Strings;
use Bean\ORM\Entity;


class RightBox extends Entity
{
    public $id;
    public $name;
    public $nameAllowed;
    public $text;
    public $type;
    public $sort;

    public $dateCreated;
    public $lastUpdated;



    protected function mapping()
    {
        $mapping = array(
            'lastUpdated' => array('timeStamp' => true),
            'dateCreated' => array('timeStamp' => true)
        );

        return $mapping;
    }
}
