<?php

namespace Model\Entity;


use Nette;
use Nette\Utils\Strings;
use Bean\ORM\Entity;


class Cart extends Entity
{
	public $id;
	public $userId;
	public $quantity;
	public $productId;
	public $cartOrderId;

	public $dateCreated;
    public $lastUpdated;



	protected function mapping()
	{
		$mapping = array(
			'lastUpdated' => array('timeStamp' => true),
			'dateCreated' => array('timeStamp' => true)
		);

		return $mapping;
	}
}
