<?php

namespace Model\Entity;


use Nette;
use Nette\Utils\Strings;
use Bean\ORM\Entity;


class CartOrder extends Entity
{
    const CART = 'cart';
    const ADDRESS = 'address';
    const SHIPPING = 'shipping';
    const PAYMENT = 'payment';

    public $id;
    public $deliveryCountry;
    public $deliveryCity;
    public $deliveryAddress;
    public $deliveryZip;

    public $billingCountry;
    public $billingCity;
    public $billingAddress;
    public $billingZip;

    public $shipping;
    public $payment;
    public $state;

    public $dateCreated;
    public $lastUpdated;


    protected function mapping()
    {
        $mapping = array(
            'lastUpdated' => array('timeStamp' => true),
            'dateCreated' => array('timeStamp' => true)
        );

        return $mapping;
    }


}
