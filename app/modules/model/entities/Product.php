<?php

namespace Model\Entity;


use Nette;
use Nette\Utils\Strings;
use Bean\ORM\Entity;


class Product extends Entity
{
	public $id;
	public $name;
	public $slug;
	public $categoryId;
	public $price;
	public $status;
	public $stock;
	public $description;
	public $keywords;

	public $dateCreated;
    public $lastUpdated;



	protected function mapping()
	{
		$mapping = array(
			'lastUpdated' => array('timeStamp' => true),
			'dateCreated' => array('timeStamp' => true),
		);

		return $mapping;
	}
}
