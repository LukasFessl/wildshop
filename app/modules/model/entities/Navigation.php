<?php

namespace Model\Entity;


use Nette;
use Nette\Utils\Strings;
use Bean\ORM\Entity;


class Navigation extends Entity
{
	public $id;
	public $parentId;
	public $name;
	public $slug;
    public $text;
    public $type;
    public $marginLeft;
    public $size;
	public $sort;
	public $keywords;
	public $description;

	public $dateCreated;
    public $lastUpdated;



	protected function mapping()
	{
		$mapping = array(
			'lastUpdated' => array('timeStamp' => true),
			'dateCreated' => array('timeStamp' => true)
		);

		return $mapping;
	}
}
