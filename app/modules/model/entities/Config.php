<?php

namespace Model\Entity;


use Nette;
use Nette\Utils\Strings;
use Bean\ORM\Entity;


class Config extends Entity
{
    public $id;
    public $name;
    public $value;

    public $dateCreated;
    public $lastUpdated;



    protected function mapping()
    {
        $mapping = array(
            'lastUpdated' => array('timeStamp' => true),
            'dateCreated' => array('timeStamp' => true),
            'index' => 'name'
        );

        return $mapping;
    }
}
