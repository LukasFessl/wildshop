<?php

namespace Model\Entity;


use Nette;
use Nette\Utils\Strings;
use Bean\ORM\Entity;


class ProductImage extends Entity
{
	public $id;
	public $slug;
	public $productId;
    public $title;

	public $dateCreated;
    public $lastUpdated;



	protected function mapping()
	{
		$mapping = array(
			'lastUpdated' => array('timeStamp' => true),
			'dateCreated' => array('timeStamp' => true)
		);

		return $mapping;
	}
}
