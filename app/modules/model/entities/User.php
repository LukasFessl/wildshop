<?php

namespace Model\Entity;


use Nette;
use Nette\Utils\Strings;
use Bean\ORM\Entity;


class User extends Entity
{
	public $id;
	public $firstName;
	public $lastName;
	public $email;
	public $password;
	public $enabled;
	public $role;

	public $country;
	public $city;
	public $address;
	public $zip;

	public $dateCreated;
    public $lastUpdated;



	protected function mapping()
	{
		$mapping = array(
			'lastUpdated' => array('timeStamp' => true),
			'dateCreated' => array('timeStamp' => true)
		);

		return $mapping;
	}


	public function setPassword($password)
	{
		$this->password = \AdminModule\HashPassword::hash($password);
	}



	public function emailExist($id = NULL, $email)
	{
		if($id)
			$res = $this->countByNotIdAndEmail($id, $email)->count;
		else
			$res = $this->countByEmail($email)->count;
		return $res == 0 ? false : true;
	}

}
