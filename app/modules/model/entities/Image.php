<?php

namespace Model\Entity;


use Nette;
use Nette\Utils\Strings;
use Bean\ORM\Entity;


class Image extends Entity
{
	const SLIDER = "slider";
	const FILE = "file";

	public $id;
	public $name;
	public $slug;
    public $type;
	public $sort;

	public $dateCreated;
    public $lastUpdated;



	protected function mapping()
	{
		$mapping = array(
			'lastUpdated' => array('timeStamp' => true),
			'dateCreated' => array('timeStamp' => true)
		);

		return $mapping;
	}


}
