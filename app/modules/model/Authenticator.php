<?php

namespace Model;

use Nette;
use Nette\Security\IAuthenticator;
use Nette\Security\AuthenticationException;
use Nette\Security\Identity;
use Bean\ORM\Create;

class Authenticator implements IAuthenticator
{

	protected $connection;

	public function __construct(Nette\Database\Context $connection)
	{
		$this->connection = $connection;
	}



	public function authenticate(array $credentials)
	{
		list($email, $password, $role) = $credentials;

		$res = $this->connection->table('user')
								->where(array('email' => $email, 'password' => HashPassword::hash($password), 'role' => $role))
								->select('id, email, password, role, enabled')->fetch();
		if(!$res)
			throw new AuthenticationException('User or password is bad', IAuthenticator::INVALID_CREDENTIAL);

		return new Identity($res->id, $res->role, $res);
	}
}
