<?php

namespace App;

use Nette,
	Nette\Application\Routers\RouteList,
	Nette\Application\Routers\Route,
	Nette\Application\Routers\SimpleRouter;


/**
 * Router factory.
 */
class RouterFactory
{

	/**
	 * @return \Nette\Application\IRouter
	 */
	public function createRouter()
	{
		$router = new RouteList();

		$router[] = new Route('admin/', array(
			'module' => 'admin',
			'presenter' => 'Auth',
			'action' => 'default'
		));


		$router[] = new Route('admin/<presenter>/<action>[/<id>]', array(
			'module' => 'admin',
			'presenter' => 'Homepage',
			'action' => 'default'
		));






		$router[] = new Route('/user/auth', array(
			'module' => 'front',
			'presenter' => 'User',
			'action' => 'auth'
		));

		$router[] = new Route('/user/registration', array(
			'module' => 'front',
			'presenter' => 'User',
			'action' => 'registration'
		));


        $router[] = new Route('/order[/<step>]', array(
            'module' => 'front',
            'presenter' => 'order',
            'action' => 'default'
        ));



		$router[] = new Route('/product/<productSlug>', array(
			'module' => 'front',
			'presenter' => 'Product',
			'action' => 'default'
		));


		$router[] = new Route('/<page>[/<subpage>]', array(
			'module' => 'front',
			'presenter' => 'Homepage',
			'action' => 'default'
		));



		$router[] = new Route('<presenter>/<action>[/<id>]', array(
			'module' => 'front',
			'presenter' => 'Homepage',
			'action' => 'default'
		));





    	$router[] = new Route('<presenter>/<action>[/<id>]', 'Homepage:default');

		return $router;
	}

}
