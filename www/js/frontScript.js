$( document ).ready(function() {

	$.nette.init();


	$("#slider").cycle({
	    fx:      'fade',
	    speed:    1500,
	    timeout:  6000,
	    pager:    '#headNav',
	    slideExpr:'.sliderImg',
		next:   '#next',
    	prev:   '#prev'
	});


	// $("input[name^='deliver']").change(function() {
	// 	$.get('/eshop/cart/?do=deliver', {'id': $(this).val()})
	// 		.done(function(data){
	// 			if (data.addressStatus == 1)
	// 	    		$(".address").show();
	// 	    	else
	// 	    		$(".address").hide();
	// 		});
	// });

});
