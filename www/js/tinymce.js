$( document ).ready(function() {

    initMce('textarea_mce_navigation', 900, 200);


    function initMce(e, w, h) {
        tinymce.init({
            mode : "specific_textareas",
            elements : e,
            width : w,
            height: h,
            fontsize_formats: "8px 10px 12px 14px 16px 18px 20px 22px 24px 28px 36px",
            resize: "both",
            menubar : false,

            file_browser_callback : function(field_name, url, type, win) {
                buildFileBrowser(field_name, url, type, win);
            },


            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste textcolor "
            ],

            toolbar1: "undo redo | bold italic underline strikethrough | formatselect fontselect fontsizeselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
            toolbar2: "cut copy paste table | link unlink image media anchor | forecolor backcolor  | subscript superscript code "
        });
    };


    function buildFileBrowser(field_name, url, type, win){
        tinymce.activeEditor.windowManager.open({
            title: 'Image Browser',
            url: "/admin/fileManager/default/?field=" + field_name + "&url=" + url,
            width: 800,
            height: 550,
        }, {
            oninsert: function (url) {
                win.document.getElementById(field_name).value = url;
            }
        });

        return false;
    }

    $('img').on('click', function(event){
        var item_url =($(this).attr('src'));
        top.tinymce.activeEditor.windowManager.getParams().oninsert(item_url);
        top.tinymce.activeEditor.windowManager.close();
    });




});



